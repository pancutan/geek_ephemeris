Agregar al final del archivo ~/.bashrc (o el equivalente al shell que esté utilizando. Por ejemplo en zsh sería ~/.zshrc) la siguiente línea:

~/geek_ephemeris/dayValidator.sh

y guardar en el home de su usuario el directorio geek_ephemeris
