#!/bin/bash
DATE=`date +"%d-%m"`
DAYOFYEAR=`date +%j`
DAYOFWEEK=`date +%u`
DAYOFMONTH=`date +%d`
MONTHNUMBER=`date +%m`

#VALIDO QUE SEA DÍA 256 DEL AÑO
if test "$DAYOFYEAR" == "256";
then
       PROGRAMERDAY=`date +"%d-%m"` 
else
       
       PROGRAMERDAY="01-01-0001"
fi

#VALIDO QUE SEA ULTIMO VIERNES DE JULIO
if [ $MONTHNUMBER -eq 07 ] && [ $DAYOFWEEK -eq 5 ] && [ $DAYOFMONTH -gt 24 ]
then
	SYSADMINDAY=`date +"%d-%m"`
else

	SYSADMINDAY="01-01-0001"
fi

#CASE PARA VER QUE DIBUJO MOSTRAR
case $DATE in
	14-03)
		cat ~/geek_ephemeris/welcomePictures/pi2 | lolcat
		echo ""
		echo "Feliz día de Pi" | lolcat
		echo ""
	;;
	25-03)
                cat ~/geek_ephemeris/welcomePictures/gandalf2 | lolcat
                echo ""
		echo "Feliz día de Tolkien" | lolcat
		echo ""
        ;;
	04-05)
                cat ~/geek_ephemeris/welcomePictures/starwars | lolcat
                echo ""
		echo "Feliz día de Star Wars" | lolcat
                echo "May the 4th - May the Force be with you" | lolcat
		echo ""
        ;;
	10-05)
                cat ~/geek_ephemeris/welcomePictures/garrote | lolcat
                echo ""
		echo "Feliz día del garrote" | lolcat
		echo ""
        ;;
	25-05)
                cat ~/geek_ephemeris/welcomePictures/friki | lolcat
                echo ""
		echo "Feliz día del Orgullo Friki" | lolcat
		echo ""
        ;;
	19-09)
                cat ~/geek_ephemeris/welcomePictures/pirata | lolcat
                echo ""
		echo "Feliz día Internacional de Hablar como un Pirata" | lolcat
		echo ""
        ;;
	29-08)
                cat ~/geek_ephemeris/welcomePictures/joystick | lolcat
		echo ""
		echo "Feliz día mundial del Gamer" | lolcat
		echo ""
        ;;
	31-08)
                cat ~/geek_ephemeris/welcomePictures/nfs | lolcat
                echo ""
		echo "Lanzamiento de The Need for Speed " | lolcat
		echo ""
        ;;
	15-09)
                cat ~/geek_ephemeris/welcomePictures/batman | lolcat
                echo ""
		echo "Feliz día mundial de Batman" | lolcat
		echo ""
        ;;
	22-09)
                cat ~/geek_ephemeris/welcomePictures/hobbit | lolcat
                echo ""
		echo "Feliz día del Hobbit" | lolcat
		echo ""
        ;;
	21-10)
                cat ~/geek_ephemeris/welcomePictures/backToTheFuture | lolcat
                echo ""
		echo "Feliz día de Volver al Futuro" | lolcat
		echo ""
        ;;
	27-02)
                cat ~/geek_ephemeris/welcomePictures/pokemon | lolcat
                echo ""
		echo "Feliz día de Pokemon" | lolcat
		echo ""
        ;;
	02-04)
                cat ~/geek_ephemeris/welcomePictures/lego | lolcat
                echo ""
		echo "Feliz día mundial de LEGO" | lolcat
		echo ""
        ;;
	09-05)
                cat ~/geek_ephemeris/welcomePictures/goku2 | lolcat
                echo ""
		echo "Feliz día de Goku" | lolcat
		echo ""
        ;;
	$PROGRAMERDAY)
#		cat ~/geek_ephemeris/welcomePictures/programerday | lolcat
		#echo "día del programador"
	;;	
	$SYSADMINDAY)
		cat ~/geek_ephemeris/welcomePictures/sysadminday | lolcat
                #echo ""
		#echo "Feliz día del SYSADMIN"
		echo ""
        ;;
	*)
		cat ~/geek_ephemeris/welcomePictures/submarine | lolcat
       		echo "Cyclops: Welcome aboard captain. All systems online." | lolcat
		echo ""
	;;
esac

